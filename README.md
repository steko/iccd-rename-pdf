# Rinomina PDF di schede ICCD con il loro codice NCT

Script di rinomina dei file PDF esportati dalla funzione _Stampa_ di SIGECweb.

Lo script per facilità si basa su un primo passaggio svolto tramite l'utility
pdftotext, eseguita con il seguente comando:

    for i in *.pdf; do pdftotext -raw "$i"; done

oppure su Windows:

    for i in *.pdf; do pdftotext -raw "$i"; done
        
che crea gli opportuni file txt di elaborazione intermedia.

In base ai contenuti dei file txt, lo script rinomina i file PDF contenuti
nella directory indicata, tenendo conto anche di schede con relazioni verticali.

Il codice `NCT` è preceduto dalla sigla `TSK` che identifica il tipo di scheda.

## Esempi

- scheda OA con `NCTN` 00069704: `OA_0700069704.pdf`
- scheda OA con `NTCN` 00069719, `NCTS` A e `RVEL` 1: `OA_0700069719_A-1.pdf`
