#! /usr/bin/env python3

"""
Script di rinomina dei file PDF esportati dalla funzione _Stampa_ di SIGECweb.

Lo script per facilità si basa su un primo passaggio svolto tramite l'utility
pdftotext, eseguita con il seguente comando:

    for i in *.pdf; do pdftotext -raw "$i"; done

oppure su Windows:

    for i in *.pdf; do pdftotext.exe -raw "$i"; done
        
che crea gli opportuni file txt di elaborazione intermedia.

In base ai contenuti dei file txt, lo script rinomina i file PDF contenuti
nella directory indicata.

Copyright 2018 Soprintendenza ABAP Liguria

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from pathlib import Path

p = Path('./Schede PDF')

def print_to_nct(txt_file):
    """Read a few lines from the txt file and returns the corresponding value."""
    
    with open(txt_file) as txt:
        lines = list(txt.readlines())
        tsk = lines[3].split()[-1]
        nctr = lines[6].split()[-1]
        nctn = lines[9].strip('\n')
        newfilename = 'Schede PDF/{}_{}{}'.format(tsk, nctr, nctn)
        if lines[10].startswith('NCTS'):
            ncts = lines[12].strip('\n')
            newfilename = '{}_{}'.format(newfilename, ncts)
        if lines[15].startswith('RV - RELAZIONI'):
            if lines[17].startswith('RVEL - Livello'):
                rvel = lines[17].split()[-1]
                newfilename = '{}-{}'.format(newfilename, rvel)
        newfilename = '{}.pdf'.format(newfilename)
        return newfilename

def main():
    for i in p.glob('*.txt'):
        pdf_file = i.with_suffix('.pdf')
        if pdf_file.exists():
            try:
                pdf_file.rename(print_to_nct(i))
            except FileExistsError as e:
                print(e)
        else:
            print(print_to_nct(i))


if __name__ == '__main__':
    main()
